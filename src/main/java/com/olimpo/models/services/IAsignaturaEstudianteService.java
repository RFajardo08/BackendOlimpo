package com.olimpo.models.services;

import java.util.List;

import com.olimpo.models.entity.AsignaturaEstudiante;

public interface IAsignaturaEstudianteService {

	public List<AsignaturaEstudiante> findAll();
}
