package com.olimpo.models.services;

import java.util.List;

import com.olimpo.models.entity.Asignatura;

public interface IAsignaturaService {

	public List<Asignatura> findAll();
}
