package com.olimpo.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.olimpo.models.dao.IColegioDao;
import com.olimpo.models.entity.Colegio;

@Service
public class ColegioServiceImp implements IColegioService {

	@Autowired
	private IColegioDao colegioDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Colegio> findAll() {
		return (List<Colegio>) colegioDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Colegio findById(Integer id) {
		return colegioDao.findById(id).orElse(null);
	}

}
