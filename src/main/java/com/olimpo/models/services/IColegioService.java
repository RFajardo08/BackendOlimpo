package com.olimpo.models.services;

import java.util.List;

import com.olimpo.models.entity.Colegio;

public interface IColegioService {

	public List<Colegio> findAll();
	
	public Colegio findById(Integer id);
}
