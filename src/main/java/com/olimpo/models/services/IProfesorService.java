package com.olimpo.models.services;

import java.util.List;

import com.olimpo.models.entity.Profesor;

public interface IProfesorService {

	public List<Profesor> findAll();
	
	public Profesor findById(Integer id);
}
