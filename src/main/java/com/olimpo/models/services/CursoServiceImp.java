package com.olimpo.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olimpo.models.dao.ICursoDao;
import com.olimpo.models.entity.Curso;

@Service
public class CursoServiceImp implements ICursoService{

	@Autowired
	private ICursoDao iCursoDao;
	
	@Override
	public List<Curso> findAll() {
		return (List<Curso>) iCursoDao.findAll();
	}

}
