package com.olimpo.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.olimpo.models.dao.IProfesorDao;
import com.olimpo.models.entity.Profesor;

@Service
public class ProfesorServiceImp implements IProfesorService{

	@Autowired
	private IProfesorDao iProfesorDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Profesor> findAll() {
		return (List<Profesor>) iProfesorDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Profesor findById(Integer id) {
		return iProfesorDao.findById(id).orElse(null);
	}

}
