package com.olimpo.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olimpo.models.dao.IAsignaturaEstudianteDao;
import com.olimpo.models.entity.AsignaturaEstudiante;

@Service
public class AsignaturaEstudianteServiceImp implements IAsignaturaEstudianteService{

	@Autowired
	private IAsignaturaEstudianteDao iAsignaturaEstudianteDao;
	
	@Override
	public List<AsignaturaEstudiante> findAll() {
		return (List<AsignaturaEstudiante>) iAsignaturaEstudianteDao.findAll();		
	}
}
