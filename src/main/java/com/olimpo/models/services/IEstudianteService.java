package com.olimpo.models.services;

import java.util.List;

import com.olimpo.models.entity.Estudiante;

public interface IEstudianteService {

	public List<Estudiante> findAll();
}
