package com.olimpo.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olimpo.models.dao.IEstudianteDao;
import com.olimpo.models.entity.Estudiante;

@Service
public class EstudianteServiceImp implements IEstudianteService{

	@Autowired
	private IEstudianteDao iEstudianteDao;
	
	@Override
	public List<Estudiante> findAll() {
		return (List<Estudiante>) iEstudianteDao.findAll();
	}

}
