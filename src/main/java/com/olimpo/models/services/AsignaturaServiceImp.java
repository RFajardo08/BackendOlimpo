package com.olimpo.models.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olimpo.models.dao.IAsignaturaDao;
import com.olimpo.models.entity.Asignatura;

@Service
public class AsignaturaServiceImp implements IAsignaturaService{

	@Autowired
	private IAsignaturaDao iAsignaturaDao;
	
	@Override
	public List<Asignatura> findAll() {
		return (List<Asignatura>) iAsignaturaDao.findAll();		
	}

}
