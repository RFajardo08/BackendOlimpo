package com.olimpo.models.services;

import java.util.List;

import com.olimpo.models.entity.Curso;

public interface ICursoService {

	public List<Curso> findAll();
}
