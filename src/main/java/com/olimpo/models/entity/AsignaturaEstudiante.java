package com.olimpo.models.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "asignatura_estudiante")
public class AsignaturaEstudiante {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "asies_id")
	private int idAsignaturaEstudiante;
	
	@ManyToOne
	@JoinColumn(name = "id_estudiante")
	private Estudiante estudiante;
	
	@ManyToOne
	@JoinColumn(name = "id_asignatura")
	private Asignatura asignatura;

	public int getIdAsignaturaEstudiante() {
		return idAsignaturaEstudiante;
	}

	public void setIdAsignaturaEstudiante(int idAsignaturaEstudiante) {
		this.idAsignaturaEstudiante = idAsignaturaEstudiante;
	}

	public Estudiante getEstudiante() {
		return estudiante;
	}

	public void setEstudiante(Estudiante estudiante) {
		this.estudiante = estudiante;
	}

	public Asignatura getAsignatura() {
		return asignatura;
	}

	public void setAsignatura(Asignatura asignatura) {
		this.asignatura = asignatura;
	}

	@Override
	public int hashCode() {
		return Objects.hash(idAsignaturaEstudiante);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AsignaturaEstudiante other = (AsignaturaEstudiante) obj;
		return idAsignaturaEstudiante == other.idAsignaturaEstudiante;
	}
	
	
}
