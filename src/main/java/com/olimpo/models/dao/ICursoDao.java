package com.olimpo.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.olimpo.models.entity.Curso;

public interface ICursoDao extends CrudRepository<Curso, Integer> {

}
