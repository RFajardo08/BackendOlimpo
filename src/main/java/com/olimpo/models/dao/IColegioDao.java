package com.olimpo.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.olimpo.models.entity.Colegio;

public interface IColegioDao extends CrudRepository<Colegio, Integer> {

}
