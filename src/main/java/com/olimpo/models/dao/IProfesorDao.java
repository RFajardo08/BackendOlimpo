package com.olimpo.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.olimpo.models.entity.Profesor;

public interface IProfesorDao extends CrudRepository<Profesor, Integer>{

}
