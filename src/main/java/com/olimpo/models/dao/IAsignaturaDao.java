package com.olimpo.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.olimpo.models.entity.Asignatura;

public interface IAsignaturaDao extends CrudRepository<Asignatura, Integer> {

}
