package com.olimpo.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.olimpo.models.entity.AsignaturaEstudiante;

public interface IAsignaturaEstudianteDao extends CrudRepository<AsignaturaEstudiante, Integer>{

}
