package com.olimpo.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.olimpo.models.entity.Estudiante;

public interface IEstudianteDao extends CrudRepository<Estudiante, Integer>{

}
