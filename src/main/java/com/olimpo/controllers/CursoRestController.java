package com.olimpo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.olimpo.models.entity.Curso;
import com.olimpo.models.services.ICursoService;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class CursoRestController {

	@Autowired
	private ICursoService iCursoService;
	
	@GetMapping("/curso")
	public List<Curso> index(){
		return iCursoService.findAll();
	}
}
