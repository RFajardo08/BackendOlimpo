package com.olimpo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.olimpo.models.entity.AsignaturaEstudiante;
import com.olimpo.models.services.IAsignaturaEstudianteService;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class AsignaturaEstudianteRestController {

	@Autowired
	private IAsignaturaEstudianteService iAsignaturaEstudianteService;
	
	@GetMapping("/asiest/{id}")
	public List<AsignaturaEstudiante> index(){
		return iAsignaturaEstudianteService.findAll();
	}
}
