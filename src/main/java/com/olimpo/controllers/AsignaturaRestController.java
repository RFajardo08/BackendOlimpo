package com.olimpo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.olimpo.models.entity.Asignatura;
import com.olimpo.models.services.IAsignaturaService;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class AsignaturaRestController {

	@Autowired
	private IAsignaturaService iAsignaturaService;
	
	@GetMapping("/asignatura/{id}")
	public List<Asignatura> index(){
		return iAsignaturaService.findAll();
	}
}
