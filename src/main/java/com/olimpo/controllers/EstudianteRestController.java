package com.olimpo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.olimpo.models.entity.Estudiante;
import com.olimpo.models.services.IEstudianteService;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class EstudianteRestController {

	@Autowired
	private IEstudianteService iEstudianteService;
	
	@GetMapping("/estudiante")
	public List<Estudiante> index(){
		return iEstudianteService.findAll();
	}
}
