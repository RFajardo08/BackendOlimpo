package com.olimpo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.olimpo.models.entity.Profesor;
import com.olimpo.models.services.IProfesorService;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class ProfesorRestController {

	@Autowired
	private IProfesorService iProfesorService;
	
	@GetMapping("/profesor")
	public List<Profesor> index(){
		return iProfesorService.findAll();
	}
	
	@GetMapping("/profesor/{id}")
	public Profesor show(@PathVariable Integer id) {
		return iProfesorService.findById(id);
	}
}
