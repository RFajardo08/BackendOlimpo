package com.olimpo.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.olimpo.models.entity.Colegio;
import com.olimpo.models.services.IColegioService;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class ColegioRestController {

	@Autowired
	private IColegioService iColegioService;
	
	@GetMapping("/colegio")
	public List<Colegio> index(){
		return iColegioService.findAll();
	}
	
	@GetMapping("/colegio/{id}")
	public Colegio show(@PathVariable Integer id) {
		return iColegioService.findById(id);
	}
}
